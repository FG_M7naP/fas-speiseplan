# Installation:  
`sudo apt-get install python3 python3-bs4 python3-requests python3-pip`  
`chmod +x FAS.py`  

# Start:  
`./FAS.py`

Das Script ist in Python3 geschrieben. Insofern hoffe ich das die Shebang im Script auf den richtigen Interpreter verweist. 

Es kann sein das evt noch ein anderes Python-Paket (zB configparser) benötigt wird. Ich habe hier gerade keine frische Entwicklungsumgebung in der ich das testen kann. Sorry. Für die Installation von Paketen welche die Debianpaketverwaltung nicht bereitstellt, kann das Pythonpaketmanagment `pip` genutzt werden.  
[Anleitung](https://linuxize.com/post/how-to-install-pip-on-debian-10/)

Ich habe mich beim schreiben des Scripts an die allgemeinen Syntaxgewohnheiten [PEP8](https://www.python.org/dev/peps/pep-0008/) gehalten. 

Viele Beispiele die im Internet zu finden sind operieren mit älteren Paketen oder eigenarten von python2. Deshalb hier zwei Sachen die ich kurz erklären möchte: 

Eine schöne und einfache neuerung in python 3 ist die "String Formatting Syntax". die beiden alten methoden `%-formatting` sowie das `str.format()` sind damit weit seltener anzuwenden (gibt immernoch Situatonen in denen das sicher günstig ist). Grundsätzlich sollte man sich also die [f-strings](https://docs.python.org/3/reference/lexical_analysis.html#f-strings) angewöhnen. 

`pathlib` ist ein mächtiges werkzeug und nicht so 'eigenartig' wie das in die jahre gekommene `os`. Operationen auf Dateisystemebene werden Betriebssystemspezifisch zuverlässig umgesetzt. Lohnt sich. [pathlib](https://docs.python.org/3/library/pathlib.html)