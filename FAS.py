#!/usr/bin/env python3

# python-pakete die wir in dem script nutzen
import requests  # paket um http-requests in python abzuwickeln
from urllib.parse import urljoin  # urls zusammenfügen
from bs4 import BeautifulSoup  # webscraping-tool um die daten ohne browser rauszufrimeln
import configparser  # zentrale einstellungen außerhalb des scriptes in eine conig.ini auslagern
from pathlib import Path  # pfade verarbeiten und operationen auf dateisystemebene vornehmen
import re  # reguläre ausdrücke


def main():
    print("Download des FAS-Speiseplans")

    # definition der relevanten urls
    url_fas_base = "https://www.fas-dresden.de/"
    url_fas_lgin = "https://www.fas-dresden.de/start"
    url_fas_speisepl = "https://www.fas-dresden.de/intern/schulessen/speiseplan"

    # per BeautifulSoup eine session starten um den login zu realisieren und die speiseplanseite parsen
    s = requests.Session()
    s.post(url_fas_lgin, data={"do": "login", "u": userName, "p": PassW})
    r = s.get(url_fas_speisepl)
    soup = BeautifulSoup(r.text, "html.parser")

    # in den daten der speiseplanseite nach einem bestimmten link suchen und die volle url ausgeben
    # mit diesen angaben in soup.find (class, .jpg im link usw.) findet man eineindeutig nur den link zu dem bild
    i = soup.find("a", class_="media", href=re.compile('.jpg'), title=True)
    pic_base = i.get("href")  # nur die linkadresse ausgeben
    pic_full = urljoin(url_fas_base, pic_base)  # linkadresse mit der fas-root-adresse zusammenfügen
    print(f"Bild des Speiseplans: {pic_full}")

    p = downPath / "speiseplan.jpg"  # per pathlib die zu schreibende datei festlegen
    with p.open("wb") as f:  # datei zum schreiben (w) öffnen; statt string wird bitstream (b) erwartet
        f.write(s.get(pic_full).content)  # schreiben des contents einer GET-Anfrage der bildurl in der
        # BeautifulSoup-Session in die angegebene datei


# Programmstart als 'Standalone'
if __name__ == "__main__":
    
    # Common DIRs; uncomment for debug if your bash/shell get wrong DIRs
    cwd = Path.cwd()
    # print("Current Working DIR:", cwd)
    sdir = Path(__file__).resolve()
    wdir = sdir.parent
    # print("Script Name:", __file__)
    # print("Script DIR:", wdir)
    config_file = wdir / "config.ini"
    # print("Config:", config_file)

    # die config.ini im selben ordner wie das script wird eingelesen
    # für die syntax der config.ini einfach dort rein schauen
    config = configparser.ConfigParser()
    config.read(config_file)

    # belegung der variablen mit den werten aus der config.ini
    downPath = config.get("DownloadDirectory", "downPath")
    downPath = Path(downPath)  # den pfad über pathlib definieren
    userName = config.get("Credentials", "userName")
    PassW = config.get("Credentials", "PassWord")

    main()  # start von main-funktion
